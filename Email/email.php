<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../assest/bootstarp/css/bootstrap.min.css">
    <script src="../assest/bootstarp/js/jquery.min.js"></script>
    <script src="../assest/bootstarp/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Login</h2>
    <form>
        <div class="form-group">
            <label for="email">Email:</label>
            <input type="email" class="form-control" id="email" placeholder="Enter email">
        </div>

        <div class="form-group">
            <label for="password">password:</label>
            <input type="password" class="form-control" id="password" placeholder="Enter password">
        </div>
        <div class="checkbox">
        <label> <input type="checkbox">   Remember me</label>

        </div>
        <button type="submit" class="btn badge">Submit</button>
    </form>
</div>

</body>
</html>